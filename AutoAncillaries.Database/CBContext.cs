﻿using AutoAncillaries.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoAncillaries.Database
{
    class CBContext: DbContext
    {

        public DbSet<Category> Category { get; set; }
        public DbSet<Products> Products{ get; set; }

    }
}
