﻿using AutoAncillaries.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoAncillaries.Database
{
    public class AAContext : DbContext
    {
        public AAContext() : base("AutoAncilariesConnection")
        {

        }
        public DbSet<Products>Products{get; set;}
        public DbSet<Category>Category{get; set;}
}
}
