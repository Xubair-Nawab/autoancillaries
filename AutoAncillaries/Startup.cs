﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AutoAncillaries.Startup))]
namespace AutoAncillaries
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
