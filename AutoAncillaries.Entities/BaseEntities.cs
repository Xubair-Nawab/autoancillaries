﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoAncillaries.Entities
{
     public class BaseEntities
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int Description { get; set; }
    }
}
